<?php

require_once( 'src/checks.php' );

$a_query = "SELECT DISTINCT `book_category`.`book_id`, `books`.`title` FROM `books` LEFT JOIN (`book_category`, `category_user`)
                ON (`books`.`id` = `book_category`.`book_id` AND `book_category`.`category_id` = `category_user`.`category_id`)
            WHERE `category_user`.`user_id` = " . $_SESSION['user_id'] . " AND `books`.`available`";

$b_query = "SELECT `borrows`.`id`, `borrows`.`book_id`, `books`.`title` FROM `borrows` LEFT JOIN `books` 
            ON `borrows`.`book_id` = `books`.`id` 
            WHERE `borrows`.`user_id` = " . $_SESSION['user_id'] . " AND ISNULL(`borrows`.`returned_at`)";

$cat_query  =   "SELECT `categories`.`id`, `categories`.`cat_name` FROM `category_user` LEFT JOIN `categories`
                    ON `category_user`.`category_id` = `categories`.`id`
                WHERE `user_id` = " . $_SESSION['user_id'];

// Gather all data before including template
$available_books = $conn->query( $a_query );
$borrowed_books = $conn->query( $b_query );
$categories = $conn->query( $cat_query );

include( 'templates/dashboard.php' );
?>
