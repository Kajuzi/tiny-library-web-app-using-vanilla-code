<?php
    require_once( 'src/checks.php' );
    include( 'templates/includes/header.php' );
?>
<section>
    <h2 class="page-title">Your dashboard, <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name'] ?></h2>
    <p class="info">
        <strong>You you can borrow books in the following categories:</strong><br>
        <?php
            while( $category = $categories->fetch_object() ){
                echo '<span>' . $category->cat_name . '</span>';
            }
        ?>
    </p>
</section>
<section>
    <div class="half-width">
        <h3 class="section-title">Available Books</h3>
        <?php if( $available_books->num_rows ) ?>
        <div class="book-list">
        <?php
            while( $book = $available_books->fetch_object() ){
                echo    "<div class='book-title'>$book->title</div>
                        <div class='book-action-links'>
                        <a href='book.php?action=borrow&id=$book->id'>Borrow</a>
                        </div>";
            }
        ?>    
        </div>
    </div>
    <div class="half-width">
         <h3 class="section-title">Borrowed Books</h3>
        <?php if( $borrowed_books->num_rows ) ?>
        <div class="book-list">
        <?php
            while( $borrow = $borrowed_books->fetch_object() ){
                echo    "<div class='book-title'>$borrow->title</div>
                        <div class='book-action-links'>
                        <a href='book.php?action=bring&id=$borrow->id'>Return</a>
                        </div>";
            }
        ?>    
        </div>   
    </div>

</section>
<?php include( 'templates/includes/footer.php' );