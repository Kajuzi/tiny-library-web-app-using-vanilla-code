<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Library Management System</title>
    <link rel="stylesheet" href="assets/css/app.css">
</head>
<body>
<header>
    <div class="logo">Library Management System</div>
    <div class="nav-links">
        <ul>
            <li><a href="dashboard.php">Dashboard</a></li>
            <?php if ( isset( $_SESSION['user_id'] ) ): ?>
            <li><a href="logout.php">Logout</a></li>
            <?php endif; ?>
        </ul>
    </div>
</header>