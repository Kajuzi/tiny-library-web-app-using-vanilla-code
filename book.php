<?php

require_once( 'src/checks.php' );

extract( $_GET );

// Validate input
if ( !isset( $action ) ){
    exit( "It's not clear what you want to be done. Please pass an action as a Get parameter");
} elseif ( !function_exists( $action ) ) {
    exit( "Invalid action. Try 'borrow' or 'bring'" );
} elseif( !isset( $id ) or !is_numeric( $id ) ) {
    exit( 'Please provide a valid id as a get parameter. An integer is expected' );
} else {
    $action( (int) $id, $conn ); // call_user_func the lazy way
}

/**
 * Borrow a book from the library for the logged in user
 *
 * @param integer $id
 * @param object $conn
 * @return void
 */
function borrow( $id, $conn )
{
    if (!can_borrow($id, $conn))
        exit( 'You are not allowed to borrow that book' );

    $borrow = $conn->query( "INSERT INTO `borrows` (`id`, `user_id`, `book_id`, `borrowed_at`, `returned_at`) 
                            VALUES (NULL, '".$_SESSION['user_id']."', '$id', NOW(), NULL)" );

    if ( $borrow )
        $conn->query( "UPDATE `books` SET `available` = 0 WHERE `id` = $id" );
    
    header('Location: dashboard.php');
}

/**
 * Return a borrowed book
 *
 * @param integer $id
 * @param object $conn
 * @return void
 */
function bring( $id, $conn )
{
    $result = $conn->query( "SELECT `id`, `book_id` FROM `borrows` WHERE `id` = $id AND `user_id` = " . $_SESSION['user_id'] );
    $borrow = $result->fetch_object();
    if( !$result->num_rows )
        exit( 'Are you sure you borrowed that book?' );

    $bring = $conn->query( "UPDATE `borrows` SET `returned_at` = NOW() WHERE `id` = " . $id );
    if ( $bring )
        $conn->query( "UPDATE `books` SET `available` = 1 WHERE `id` = $borrow->book_id" );
    else 
        exit( 'Failed to update borrowing status' );
    
    header('Location: dashboard.php');
}

/**
 * Check if the logged in user can borrow the book
 *
 * @param integer $book_id
 * @param object $conn
 * @return boolean
 */
function can_borrow( int $book_id, $conn )
{
    $result = $conn->query( "SELECT `category_user`.`category_id`, `book_category`.`book_id` FROM category_user LEFT JOIN book_category
                            ON `category_user`.`category_id` = `book_category`.`category_id`
                            WHERE `category_user`.`user_id` = " . $_SESSION['user_id'] . " AND `book_category`.`book_id` = $book_id" );
    $row = $result->fetch_object();
    if( !$result->num_rows ) 
       return false;
    else
        return true;
}