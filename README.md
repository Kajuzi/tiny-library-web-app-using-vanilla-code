# Tiny library management system

A small library management system with a login form but no registration. User profiles have firstname, lastname and categories.
The app uses HTML, CSS, Javascript, PHP and MariaDB

Demo: [https://panlista.com/demo/library](https://panlista.com/demo/library)

### Logins
Email: user1@example.com Password: password    
Email: user2@example.com Password: password    
Email: user3@example.com Password: password    

## Installation
1. Copy files to the directory where you want the app to run from
2. Open `src/db.php` and enter the database credentials that apply
3. Import `db_dump.sql` into your database and that it

## Nice to have
* User registration
* Admin to confirm book return and condition
* Book profile seperate from copy profile so users can choose which copy they want to borrow and condition of a copy can be traced to who borrowed it
* AJAX borrow and return