<?php 

if ( isset( $_POST['email'], $_POST['password'] ) ) {

    $stmt =  $conn->stmt_init();

    if ($stmt->prepare( "SELECT * FROM users WHERE email=?" )) {
        $stmt->bind_param( "s", $_POST['email'] );
        $stmt->execute();
        $stmt->bind_result( $id, $email, $first_name, $last_name, $password );
        $stmt->fetch();

        if( $id ){
            if( password_verify( $_POST['password'], $password ) ){
                $_SESSION['user_id']    = $id;
                $_SESSION['first_name'] = $first_name;
                $_SESSION['last_name']  = $last_name;
                $_SESSION['email']      = $email;
            } else {
                exit( 'Incorrect password' );
            }
        } else {
            exit( 'Email not registered' );
        }

        $stmt->close();
    }

} elseif ( !isset( $_SESSION['user_id'] ) ) {
    require( 'templates/login-form.php' );
    exit;
} 