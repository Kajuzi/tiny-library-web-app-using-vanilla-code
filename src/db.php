<?php

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'password';
$db_name = 'db_clibrary'; 

$conn = mysqli_connect( $db_host, $db_user, $db_pass, $db_name );
if ( mysqli_connect_errno() ) {
	die ( 'Database connection failed with error: ' . mysqli_connect_error() );
}