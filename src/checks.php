<?php

/**
 * Require this file whereever the database connection and authentication is required
 */

require_once( 'db.php' );

session_start();

// Check if they are logged in
if ( !isset( $_SESSION['user_id'] ) )
    header( 'Location: index.php' );