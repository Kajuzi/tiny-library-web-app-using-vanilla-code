<?php

session_start();

define( 'CLIB', time() );

require_once( 'src/db.php' );
require_once( 'src/auth.php' );

// User is logged in. Send them to their dashboard
header( 'Location: dashboard.php' );